package com.example.appcircleicons

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.example.appcircleicons.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val imgList = listOf(
            binding.imgPowerPlant,
            binding.imgAlien,
            binding.imgFingerprint,
            binding.imgGlove,
            binding.imgDna,
            binding.imgHover,
            binding.imgFace,
            binding.imgSatelite,
            binding.imgCamera,
            binding.imgPlanet
        )

        for (element in imgList) {
            element.setOnClickListener{
                toasts(element)
                element.animate().apply {
                    duration = 1000
                    rotationYBy(360F)
                }
            }
        }
    }

    private fun toasts(view: View) {
        if (view is ImageView) {
            when (view.id) {
                R.id.imgPowerPlant -> {
                    Toast.makeText(this, ("\u2622"), Toast.LENGTH_SHORT).show()
                }
                R.id.imgAlien -> {
                    Toast.makeText(this, ("\ud83d\udc7d"), Toast.LENGTH_SHORT).show()
                }
                R.id.imgDna -> {
                    Toast.makeText(this, ("\ud83e\uddec"), Toast.LENGTH_SHORT).show()
                }
                R.id.imgFingerprint -> {
                    Toast.makeText(this, ("\ud83e\udd0c"), Toast.LENGTH_SHORT).show()
                }
                R.id.imgHover -> {
                    Toast.makeText(this, ("\ud83c\udfce"), Toast.LENGTH_SHORT).show()
                }
                R.id.imgFace -> {
                    Toast.makeText(this, ("\ud83d\udc68"), Toast.LENGTH_SHORT).show()
                }
                R.id.imgGlove -> {
                    Toast.makeText(this, ("\uD83E\uDDE4"), Toast.LENGTH_SHORT).show()
                }
                R.id.imgSatelite -> {
                    Toast.makeText(this, ("\uD83D\uDEF0"), Toast.LENGTH_SHORT).show()
                }
                R.id.imgCamera -> {
                    Toast.makeText(this, ("\uD83D\uDCF7"), Toast.LENGTH_SHORT).show()
                }
                R.id.imgPlanet -> {
                    Toast.makeText(this, ("\uD83E\uDE90"), Toast.LENGTH_SHORT).show()
                }
            }

        }

    }
}